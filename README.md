# JavaScript Snake Game

This project is based on the [Snake implementation in pure JavaScript by Patrojk](https://github.com/patorjk/JavaScript-Snake).

The game was extended to work with a Tensorflow.js model, trained with the [Teachable Machine](https://teachablemachine.withgoogle.com), as a controller. The adaptions in the UI as well as the integration of the required libraries are already done for you. It's now up to you, to train a suitable model and integrated it with the game...

**Enjoy...!**

## Link you model

Head over to the [Teachable Machine](https://teachablemachine.withgoogle.com) and train a model. This project is prepared to work with a **pose model** out of the box. It should although be possible to use an image based model. One might even try to integrate a sound based model?

After you trained your model, publish (upload) it as a `Tensorflow.js` model. Copy the public link and paste it as value of the `URL` constant in the file [`js/tm.js`](js/tm.js) on line #5.

## Fiddle with the game

The game should still work as expected - you may control it with the arrow keys and pause with the space key.

It's your task to integrate your previously trained model to control the game. Inspect the existing code and/or fiddle with the game in the browser (developer console).

Some hints to get you started:

- There's a variable called `mySnakeBoard` initialized in the file [`js/init.js`](js/init.js).
- You can get the `Snake` object from the `Board` instance using the public `getSnake` method.
- The arrow keys are handled within a method called `handleArrowKeys`.